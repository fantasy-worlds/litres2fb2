from cx_Freeze import setup, Executable

setup(name="LitresToFb2",
      version="0.1",
      description="Generate a fb2 from litres reader",
      executables=[Executable("litres2fb2.py")])
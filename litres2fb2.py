import base64
import hashlib
import html
import itertools
import os
import urllib.parse as urlparse
from getpass import getpass
from pathlib import Path
from urllib.parse import parse_qs

import requests
import time
from colorama import init
from lxml import etree
from termcolor import colored
from tqdm import tqdm

import relaxedjson

'''
8. Проверка валидатором полученного файла'''


class ReaderToFB2:

    def __init__(self):
        init()

    FILE_CACHE = True
    CACHE_DIR = Path("cache")
    LITRES_BASE = 'https://www.litres.ru'
    LOGIN_URL = LITRES_BASE + '/pages/login/'
    HEADERS = {  # Requests headers
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
    }
    s = requests.Session()  # Create new session
    ATTACHMENTS = []
    NOTES = []
    NOTES_COUNT = 0
    TAG_MAP = {
        'em': 'emphasis',
        'br': 'empty-line',
        'img': 'image',
        'note': 'a',
        'blockquote': 'cite'
    }

    @staticmethod
    def timestamp():  # Timestamp in Litres format function
        return str(int(time.time() * 1000))

    def litres_login(self):
        csrf = ""
        login = input(colored("Litres login: ", "magenta"))
        password = getpass(colored("Litres password: ", "magenta"))
        print("Getting CSRF...")
        loginData = {
            'csrf': '',
            'login': login,
            'pre_action': 'login',
            'pwd': password,
            'ref_url': '/',
            'showpwd': 'on',
            'timestamp': self.timestamp(),
            'utc_offset_min': '180',
        }

        payload = self.s.post(self.LOGIN_URL, data=loginData, headers=self.HEADERS,
                              allow_redirects=False).text  # Request CSRF data

        for i in range(0, len(payload)):
            if payload.find("\"csrf\"", i, i + 6) != -1:
                csrf_start = payload.find("\"", i + 7) + 1
                csrf_end = payload.find("\"", csrf_start + 1)
                csrf = payload[csrf_start:csrf_end]
                print(colored("CSRF: " + csrf, 'yellow'))
                break

        if csrf == "":
            exit(colored("CSRF not found!", 'red'))

        print("Logging in...")

        loginData = {
            'csrf': csrf,
            'login': login,
            'pre_action': 'login',
            'pwd': password,
            'ref_url': '/',
            'showpwd': 'on',
            'timestamp': self.timestamp(),
            'utc_offset_min': '180',
        }

        payload = self.s.post(self.LOGIN_URL, data=loginData, headers=self.HEADERS,
                              allow_redirects=False).text  # Login request

        if payload == "":
            print(colored("Logged in!", 'green'))
        else:
            exit(colored(payload + "\n Login error!", 'red'))

    def get_description(self, meta):
        # TODO: download description from trial fb2
        # https://www.litres.ru/gettrial/?art={ID}&format=fb2&lfrom=0
        description = etree.Element('description')

        title_info = etree.Element('title-info')

        for author in filter(lambda x: x['Role'] == 'author', meta['Authors']):
            a = etree.Element('author')
            if 'First' in author:
                first = etree.Element('first-name')
                first.text = author['First']
                a.append(first)
            if 'Last' in author:
                first = etree.Element('last-name')
                first.text = author['Last']
                a.append(first)
            title_info.append(a)

        for author in filter(lambda x: x['Role'] == 'translator', meta['Authors']):
            a = etree.Element('translator')
            if 'First' in author:
                first = etree.Element('first-name')
                first.text = author['First']
                a.append(first)
            if 'Last' in author:
                first = etree.Element('last-name')
                first.text = author['Last']
                a.append(first)
            title_info.append(a)

        title = etree.Element('book-title')
        title.text = meta['Title']
        title_info.append(title)

        # TODO: wrap in <p>
        if 'Annotation' in meta:
            annotation = etree.Element('annotation')
            annotation.text = meta['Annotation']
            title_info.append(annotation)

        if 'Written' in meta:
            if 'Date' in meta['Written']:
                date = etree.Element('date')
                date.text = meta['Written']['Date']
                title_info.append(date)

        coverpage = etree.Element('coverpage')

        lang = etree.Element('lang')
        lang.text = meta['Lang']

        title_info.append(lang)

        if 'Written' in meta:
            if 'Lang' in meta['Written']:
                srclang = etree.Element('src-lang')
                srclang.text = meta['Written']['Lang']
                title_info.append(srclang)

        image = etree.Element('image')
        image.set('{http://www.w3.org/1999/xlink}href', '#cover.jpg')
        self.ATTACHMENTS.append('cover.jpg')
        coverpage.append(image)

        title_info.append(coverpage)

        if 'Sequences' in meta:
            for sequence in meta['Sequences']:
                s = etree.Element('sequence')
                s.set('name', sequence)
                title_info.append(s)

        description.append(title_info)

        document_info = etree.Element('document-info')

        id = etree.Element('id')
        id.text = meta['UUID']

        document_info.append(id)

        version = etree.Element('version')
        version.text = meta['version']

        document_info.append(version)
        description.append(document_info)

        return description

    @staticmethod
    def join_text_chunks(s):
        return ''.join(s).replace('\xad', '')

    def append_text(self, root, text):
        if len(root) > 0:
            root[-1].tail = self.join_text_chunks(text)
        else:
            root.text = self.join_text_chunks(text)

    def get_element(self, data):
        tag = data['t']
        tag = self.TAG_MAP.get(tag, tag)

        root = etree.Element(tag)

        row = data['c'] if 'c' in data else None
        href = data['s'] if 's' in data else None
        role = data['role'] if 'role' in data else None

        if row is not None:
            text = []
            if tag == 'cite':
                for el in row:
                    if el['t'] == 'title':
                        el['t'] = 'subtitle'
                    if el['t'] == 'subscription':
                        el['t'] = 'text-author'
            for item in row:
                if isinstance(item, str):
                    text.append(item)
                elif isinstance(item, dict):
                    self.append_text(root, text)
                    root.append(self.get_element(item))
                    text = []
                else:
                    raise RuntimeError
            self.append_text(root, text)

        if href is not None:
            root.set('{http://www.w3.org/1999/xlink}href', f'#{href}')
            self.ATTACHMENTS.append(href)

        if role is not None and role == 'footnote' and 'f' in data:
            note_id = f'n_{self.NOTES_COUNT+1}'
            root.set('{http://www.w3.org/1999/xlink}href', f'#{note_id}')
            self.NOTES_COUNT += 1
            root.set('type', 'note')
            footnote = data['f']
            footnote['t'] = 'section'
            processed_footnote = self.get_element(footnote)
            processed_footnote.set('id', note_id)
            self.NOTES.append(processed_footnote)

        return root

    def process_array(self, arr):
        return self.get_element({"t": "section", "c": arr})

    def get_litres_file(self, link, is_text=True):
        has_cache = False
        cache_filename = ''
        self.CACHE_DIR.mkdir(parents=True, exist_ok=True)
        if self.FILE_CACHE:
            h = hashlib.new('ripemd160')
            h.update(link.encode())
            cache_filename = h.hexdigest()
            if os.path.isfile(self.CACHE_DIR / cache_filename):
                has_cache = True
        if not has_cache:
            response = self.s.get(url=link)
            if not response.ok:
                if is_text:
                    exit(colored(f'Returned error {response.status_code}: {response.reason}'))
                else:
                    return None
            if is_text:
                response_text = response.text
            else:
                response_text = response.content
            with open(self.CACHE_DIR / cache_filename, 'w+' if is_text else 'wb+', encoding='utf-8' if is_text else None) as cache:
                cache.write(response_text)
            return response_text
        else:
            with open(self.CACHE_DIR / cache_filename, 'r' if is_text else 'rb', encoding='utf-8' if is_text else None) as cache:
                return cache.read()

    def get_attachments(self, base_url):
        attaches = []
        print(colored('Downloading images…', 'cyan'))
        for a in tqdm(self.ATTACHMENTS):
            file = self.get_litres_file(f'{self.LITRES_BASE}{base_url}json/{a}', is_text=False)
            if file:
                bin = etree.Element('binary')
                bin.set("id", a)
                ct = "image/jpeg"
                if a.split('.')[1].lower() == 'png':
                    ct = "image/png"
                bin.set("content-type", ct)
                bin.text = base64.b64encode(file).decode('ascii')
                attaches.append(bin)
        print(colored('OK!', 'green'))
        return attaches

    def process_body(self, parent, content, elements):
        for i in range(int(content['s']), int(content['e']) + 1):
            if 'c' not in content \
                    or i not in list(itertools.chain.from_iterable(
                        map(lambda c: range(int(c['s']), int(c['e']) + 1), content['c']))
                    ):
                if i < len(elements):
                    parent.append(self.get_element(elements[i]))
            else:
                if i in map(lambda c: int(c['s']), content['c']):
                        section = etree.Element('section')
                        self.process_body(section, list(filter(lambda c: i == int(c['s']), content['c']))[0], elements)
                        parent.append(section)

    def process(self, base_url):
        print(colored('Downloading book…', 'cyan'))
        assert base_url[0] == '/', colored('Incorrect baseurl')
        print(colored('Downloading table of contents…', 'cyan'))
        try:
            toc = self.get_litres_file(f"{self.LITRES_BASE}{base_url}json/toc.js")
        except:
            exit(colored("Error retrieving table of contents", "red"))
        print(colored('OK!', 'green'))

        book = []
        toc_data = relaxedjson.parse(toc)

        # construct fb2

        fb2_root = etree.Element('FictionBook', nsmap={'l': 'http://www.w3.org/1999/xlink'})
        fb2_root.set('xmlns', 'http://www.gribuser.ru/xml/fictionbook/2.0')

        fb2_root.append(self.get_description(toc_data["Meta"]))

        print(colored('Downloading book parts…', 'cyan'))
        for part in tqdm(toc_data['Parts']):
            part_file = self.get_litres_file(f'{self.LITRES_BASE}{base_url}json/{part["url"]}')
            book.extend(relaxedjson.parse(part_file))
        print(colored('OK!', 'green'))

        body = etree.Element('body')
        print(colored('Processing book parts…', 'cyan'))

        self.process_body(body, toc_data['Body'][0], book)

        # for i in tqdm(range(int(max(
        #         map(lambda x: x['xp'][1], book)
        # )))):
        #     body.append(self.process_array(filter(lambda x: x['xp'][1] == i + 1, book)))
        print(colored('OK!', 'green'))

        fb2_root.append(body)

        # notes
        if len(self.NOTES) > 0:
            notes = etree.Element('body')
            notes.set('name', 'notes')
            notes_title = etree.Element('title')
            notes_title.text = 'Примечания'
            notes.append(notes_title)

            for note in self.NOTES:
                notes.append(note)

            fb2_root.append(notes)

        # attachments
        for attach in self.get_attachments(base_url):
            fb2_root.append(attach)

        path = Path("fb2")
        path.mkdir(parents=True, exist_ok=True)
        fb2_path = f'{path / toc_data["Meta"]["Title"]}.fb2'
        with open(fb2_path, 'w+', encoding='utf-8') as fb2:
            fb2.write(
                html.unescape(
                    etree.tostring(fb2_root, pretty_print=True, encoding="utf-8", xml_declaration=True).decode("utf-8"))
                    .replace("<div>", "")
                    .replace("</div>", "")
            )
        print(colored(f'Saved to {fb2_path}', 'green'))

    def start_download(self):
        self.ATTACHMENTS = []
        self.NOTES = []
        self.NOTES_COUNT = 0
        # https://www.litres.ru/static/or4/view/or.html?baseurl=/download_book/122023/52739567/&uuid=1dabbe38-b9b8-4a9e-bdc0-f4f8d309e7ce&art=122023&user=146481469&uilang=ru&catalit2&track_reading
        print(colored("Open litres reader and copy URL from browser", "blue"))
        print(colored("reader URL format is https://www.litres.ru/static/or4/view/or.html?baseurl=...", "blue"))
        reader_url = input("Reader URL:")
        parsed_reader_url = parse_qs(urlparse.urlparse(reader_url).query)
        if 'baseurl' not in parsed_reader_url or len(parsed_reader_url['baseurl']) != 1:
            exit(colored('Wrong link, no, or many baseurl', 'red'))
        self.process(parsed_reader_url['baseurl'][0])

    def start(self):
        self.litres_login()
        self.start_download()


reader = ReaderToFB2()
reader.start()
